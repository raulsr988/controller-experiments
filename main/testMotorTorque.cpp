#include "Cia402device.h"
#include "SocketCanPort.h"
#include <iostream>
#include <chrono>
#include "fcontrol.h"

int main ()
{
    //m1 setup
    SocketCanPort pm1("can0");
    CiA402SetupData sd1(2048,24,0.001, 0.144, 10.0);
    CiA402Device m1 (1, &pm1, &sd1);
    m1.Reset();
    m1.StartNode();    
    m1.SwitchOn();
    m1.Setup_Torque_Mode();

//    //m2 setup
//    SocketCanPort pm2("can0");
//    CiA402SetupData sd2(2048,24,0.001, 0.144, 10.0);
//    CiA402Device m2 (2, &pm2, &sd2);
//    m2.Reset();
//    m2.StartNode();
//    m2.SwitchOn();
//    m2.Setup_Torque_Mode();

//    //m3 setup
//    SocketCanPort pm3("can0");
//    CiA402SetupData sd3(2048,24,0.001, 0.144, 10.0);
//    CiA402Device m3 (3, &pm3, &sd3);
//    m3.Reset();
//    m3.StartNode();
//    m3.SwitchOn();
//    m3.Setup_Torque_Mode();

    // file results
    FILE *file;
    file = fopen("../data.csv","w+");
    fprintf(file, "torque, Velocity\n");

    // Sampling DTS
    double freq=100; //sensor use values: 50,100,500...
    double dts=1/freq; // 0.020s
    SamplingTime Ts;
    Ts.SetSamplingTime(dts);

    // timer
    std::chrono::system_clock::time_point start, current;

//    m1.SetTorque(0.08);
//    m2.SetTorque(0.08);
//    m3.SetTorque(0.08);
//    sleep(6);

//    m1.SetTorque(1);
//    sleep(10);

//    m1.SetTorque(0.08);
//    sleep(5);



//    for(double i=0.1; i<1; i+=0.01){

//        current = std::chrono::system_clock::now();
//        std::chrono::duration<float,std::milli> duration = current - start;

//        m1.SetTorque(i);
//        printf("Current torque: %f\n", i);
//        printf("VEL: %f\n", m1.GetVelocity());


//        //fprintf(file,"%.4f,", duration.count());
//        fprintf(file,"%.4f,",  i);
//        fprintf(file,"%.4f\n", m1.GetVelocity());

//        //sleep(1);
//        std::this_thread::sleep_for(std::chrono::milliseconds(20));

    for(double tor = 0.08; tor<1; tor+=0.01){

        for(double t=0.0; t<=0.06; t+=dts){
            if (t== 0.020) {
                m1.SetTorque(0.6);
                printf("++ Torque applied: %f\n", 0.8);
            }
            else {
                m1.SetTorque(tor);
                printf("Torque applied: %f\n", tor);
            }
            Ts.WaitSamplingTime(); // 0.020seg
        }
    }

    m1.SetTorque(0.2);
    //m2.SetTorque(0.08);
    //m3.SetTorque(0.08);

    return 1;
}
