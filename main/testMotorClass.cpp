/*
#include <iostream>
#include <fstream>
#include "Cia402device.h"
#include "CiA301CommPort.h"
#include "SocketCanPort.h"
#include "fcontrol.h"
#include <thread>
#include <mutex>


class Motor {
    private:
           SocketCanPort * pm;
           CiA402SetupData * sd;
           CiA402Device * dev;
           PIDBlock * controller;
           SamplingTime * ts;

           double targetVel= 0.0;
           std::thread thread;
           std::mutex mtx;
           void motorControllerThread();
           bool stopFlag;

    public:
        Motor(int id); // Constructor
        void configMotorController(double kp, double ki, double kd, double dts);
        void startMotorControllerThread(){thread = std::thread(&Motor::motorControllerThread, this); stopFlag=false; }
        void setMotorVelocity(double vel){this->targetVel = vel;} // modify target velocities in realtime in motorControllerThread loop
        void stopMotorController() { stopFlag = true; thread.join(); dev->SetVelocity(0); dev->ForceSwitchOff(); }
        double motorGetPosition(){ return dev->GetPosition(); }

};

Motor::Motor(int id){
    pm = new SocketCanPort ("can0"); // can0
    sd = new CiA402SetupData(2048, 24, 0.001, 0.144, 10); // neck motor configuration
    dev = new CiA402Device(id, pm, sd);
    dev->StartNode();
    dev->SwitchOn();
    dev->Setup_Velocity_Mode(1); // motor acceleration
}

void Motor::configMotorController(double kp, double ki, double kd, double dts){
    controller = new PIDBlock(kp,ki,kd,dts);
    ts = new SamplingTime();
    ts->SetSamplingTime(dts);
}


void Motor::motorControllerThread(){

    while(!stopFlag){

        mtx.lock();
        double currentVel = dev->GetVelocity();
        double velError = targetVel - currentVel;

        printf("*** current Vel: %f targetVel: %f\n",currentVel,targetVel);

        // Control process
        double cS = controller->OutputUpdate(velError);

        if (!std::isnormal(cS))
        {
            cS = 0.0;
        }
        dev->SetVelocity(cS);
        mtx.unlock();

        ts->WaitSamplingTime();

    }
}
*/

int main(){
/*
    // file results
    ofstream testingFile;
    testingFile.open("../motorthreads.csv", ofstream::out);
    testingFile << "Time, TargetVel, CurrentVel "<< endl;

    Motor m1(1);
    m1.configMotorController(0.015, 36, 0, 0.01); // ts = 0.01s = 50Hz
    m1.startMotorControllerThread();
    sleep(1);

    Motor m2(2);
    m2.configMotorController(0.015, 36, 0, 0.01); // ts = 0.01s = 50Hz
    m2.startMotorControllerThread();
    sleep(1);

    Motor m3(3);
    m3.configMotorController(0.015, 36, 0, 0.01); // ts = 0.01s = 50Hz
    m3.startMotorControllerThread();
    sleep(1);


    std::vector<int> pose {0,-5,0,5};

    for(int p=0; p<5; p++)
    {
        for(int i=0; i<pose; i++)
        {
            printf("---- moving to pose [%d]: [%f] [%f]\n", i, pose[i][0], pose[i][1]);

            pr2tendons(pose[i][0], pose[i][1], v);
            printf("tendons: [%f] [%f] [%f]\n", v[0], v[1], v[2]);

            m1.setMotorVelocity(v[0]);
            m2.setMotorVelocity(v[1]);
            m3.setMotorVelocity(v[2]);

            start = std::chrono::system_clock::now();
            //std::cout << "init pos: " << m1.GetPosition() << std::endl;

            for(double time=0; time<= steptime; time=time+dts) // totalIterations = steptime/ 0.020s(dts)
            {
                //printf("waiting... (%f)\n", time);
                imu.GetPitchRollYaw(pitch,roll,yaw);

                // rad to deg
                pitch = pitch*180/M_PI;
                roll  = roll*180/M_PI;

                printf("> IMU : (%f %f)\n", pitch, roll);

                fprintf(file,"%.2f, ",time);
                fprintf(file,"%.4f, %.4f,",  pose[i][0], pitch); // pitch target, pitch sensor
                fprintf(file,"%.4f, %.4f\n", pose[i][1], roll); // roll target,  roll sensor

            Ts.WaitSamplingTime(); // 0.020seg

            }

            //end = std::chrono::system_clock::now();
            //auto int_s = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            //std::cout << "** pos: " << m1.GetPosition() << std::endl;
            //std::cout << "Time: "<< int_s.count() <<" ms "<< std::endl;

         m1.setMotorVelocity(0);
         m2.setMotorVelocity(0);
         m3.setMotorVelocity(0);
         printf("stopped\n");
         sleep(sleeptime);

        }
    }
    // stop control
    m1.stopMotorController();
    m2.stopMotorController();
    m3.stopMotorController();
*/
    return 0;
}

