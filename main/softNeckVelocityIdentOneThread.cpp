#include "Cia402device.h"
#include "SocketCanPort.h"
#include "fcontrol.h"
#include <iostream>
#include <chrono>
#include <thread>


int main ()
{
    /*
    double targetVel = 0; // input velocity in deg/s
    targetVel = targetVel *M_PI/180; // 1 ° × π / 180 = 0,01745 rad

    //control loop sampling time
    double freq=50; //sensor use values: 50,100,500...
    double dts=1/freq;
    SamplingTime Ts;
    Ts.SetSamplingTime(dts); // 0.020

    // file results
    ofstream testingFile;
    testingFile.open("../data.csv", ofstream::out);
    testingFile << "Time, CurrentVel, VelError, ControlSignal" << endl;

    //m1 setup
    SocketCanPort pm1("can0");
    CiA402SetupData sd1(2048,24,0.001, 0.144, 10);
    CiA402Device m1 (1, &pm1, &sd1);
    m1.StartNode();
    m1.SwitchOn();
    m1.Setup_Velocity_Mode(10);

    //m2 setup
    SocketCanPort pm2("can0");
    CiA402SetupData sd2(2048,24,0.001, 0.144, 10);
    CiA402Device m2 (2, &pm2, &sd2);
    m2.StartNode();
    m2.SwitchOn();
    m2.Setup_Velocity_Mode(10);

    //m3 setup
    SocketCanPort pm3("can0");
    CiA402SetupData sd3(2048,24,0.001, 0.144, 10);
    CiA402Device m3 (3, &pm3, &sd3);
    m3.StartNode();
    m3.SwitchOn();
    m3.Setup_Velocity_Mode(10);

    // controller for motor
    PIDBlock controller_m1(0.015,36,0,dts);
    PIDBlock controller_m2(0.015,36,0,dts);
    PIDBlock controller_m3(0.015,36,0,dts);

    printf("init pos: %f\n", m1.GetPosition()*180/M_PI);
    for(double time=0; time<=10; time=+dts){

        // --- Controller 1
        double currentVel_m1 = m1.GetVelocity();
        //double velError_m1 = targetVel - currentVel;

        // Control process
        double cS_m1 = controller_m1.OutputUpdate(velError_m1);

        if (!std::isnormal(cS_m1))
        {
            cS_m1 = 0.0;
        }

        m1.SetVelocity(cS_m1);
        //printf("currentVel: (%f) velError: (%f)  controlSignal: (%f) timeLoop: (%f)\n", currentVel*180/M_PI, velError, cS, i);
        //testingFile << i << "," << currentVel*180/M_PI << "," << velError << "," << cS << "," << endl;

        // --- Controller 2
        double currentVel_m2 = m2.GetVelocity();
        double velError_m2 = targetVel - currentVel_m2;

        // Control process
        double cS_m2 = controller_m2.OutputUpdate(velError_m2);

        if (!std::isnormal(cS_m2))
        {
            cS_m2 = 0.0;
        }

        m2.SetVelocity(cS_m2);




        Ts.WaitSamplingTime();
    }    
    
    m1.SetVelocity(0);
    printf("final pos: %f\n", m1.GetPosition()*180/M_PI);

    m1.ForceSwitchOff();
    sleep(1);
    m1.PrintStatus();
    */
    return 1;

}
