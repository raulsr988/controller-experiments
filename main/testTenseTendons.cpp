#include "Cia402device.h"
#include "SocketCanPort.h"
#include <iostream>
#include <chrono>
#include <thread>
#include "fcontrol.h"

// tense tendons:
//  true: it's stopped and tensed
bool tenseThread(CiA402Device motor){
    motor.Setup_Torque_Mode();
    motor.SetTorque(0.07);
    while(motor.GetVelocity() > 0.2)
        printf("Tensing thread... vel: %f\n", motor.GetVelocity());
    motor.Setup_Velocity_Mode(0);
    return true;
}

int main ()
{
    //m1 setup
    SocketCanPort pm1("can0");
    CiA402SetupData sd1(2048,24,0.001, 0.144, 10);
    CiA402Device m1 (40, &pm1, &sd1);
    m1.Reset();
    m1.StartNode();
    m1.SwitchOn();
    m1.Setup_Velocity_Mode(10);
    
    double posdeg = 360;
    double posrad= posdeg*M_PI/180; // 1 ° × π / 180 = 0,01745 rad
    printf("Current vel: %f\n", posrad);
    m1.SetVelocity(posrad);
    sleep(3);

    if( tenseThread(m1) )
        m1.SetVelocity(1);
    sleep(3);

    return 1;
}
